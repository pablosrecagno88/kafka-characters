package com.character.consumer.demo.listener;

import com.character.consumer.demo.persistence.service.CharacterService;


import com.core.dev.characters.CharacterCreatedEvent;
import com.core.dev.characters.CreatedCharacterEventDTO;
import com.core.dev.common.Topics;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@KafkaListener(topics = Topics.CHARACTER_CREATED_EVENT_TOPIC)
public class CharacterEventHandler {

    private final CharacterService characterService;

    public CharacterEventHandler(CharacterService characterService) {
        this.characterService = characterService;
    }

    @KafkaHandler
    public void characterCreatedEventListener(
            @Payload CharacterCreatedEvent characterCreatedEvent,
            @Header(KafkaHeaders.RECEIVED_KEY) String messageKey,
            @Header("messageId") String messageId) {

        log.info("[CharacterEventHandler::characterCreatedListener] message key {}, message id {}, event {}",messageKey, messageId,characterCreatedEvent);
        characterService.saveCreatedCharacterEvent(buildCreatedCharacterEventDTO(messageId, characterCreatedEvent));
    }

    private CreatedCharacterEventDTO buildCreatedCharacterEventDTO(String messageId,CharacterCreatedEvent characterCreatedEvent) {
        return new CreatedCharacterEventDTO(messageId,characterCreatedEvent.getCharacterId(),characterCreatedEvent.getName(),characterCreatedEvent.getCharacterType());
    }
}
