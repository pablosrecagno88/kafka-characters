package com.character.consumer.demo.persistence.service;


import com.core.dev.characters.CreatedCharacterEventDTO;

public interface CharacterService {
    void saveCreatedCharacterEvent(CreatedCharacterEventDTO characterData);
}
