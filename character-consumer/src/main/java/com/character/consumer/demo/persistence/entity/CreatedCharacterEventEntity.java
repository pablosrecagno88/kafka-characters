package com.character.consumer.demo.persistence.entity;

import com.core.dev.characters.CharacterType;
import jakarta.persistence.*;

@Entity
@Table(name = "processed_created_characters_events")
public class CreatedCharacterEventEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false,unique = true)
    private String messageId;

    @Column(nullable = false, unique = true)
    private String characterId;
    @Column(nullable = false, unique = true)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private CharacterType characterType;

    public CreatedCharacterEventEntity() {
    }

    public CreatedCharacterEventEntity(String messageId, String characterId, String name, CharacterType characterType) {
        this.messageId= messageId;
        this.characterId = characterId;
        this.name = name;
        this.characterType = characterType;
    }
}
