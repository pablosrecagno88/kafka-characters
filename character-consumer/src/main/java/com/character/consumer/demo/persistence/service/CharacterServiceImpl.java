package com.character.consumer.demo.persistence.service;

import com.character.consumer.demo.error.NotRetryableException;
import com.character.consumer.demo.error.RetryableException;
import com.character.consumer.demo.persistence.entity.CreatedCharacterEventEntity;
import com.character.consumer.demo.persistence.repository.CreatedCharacterEventRepository;

import com.core.dev.characters.CreatedCharacterEventDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
public class CharacterServiceImpl implements CharacterService {

    private static final String ENDPOINT_URL = "http://localhost:8082/response/200";
    private final CreatedCharacterEventRepository createdCharacterEventRepository;
    private final RestTemplate restTemplate;

    public CharacterServiceImpl(CreatedCharacterEventRepository createdCharacterEventRepository, RestTemplate restTemplate) {
        this.createdCharacterEventRepository = createdCharacterEventRepository;
        this.restTemplate = restTemplate;
    }

    @Override
    public void saveCreatedCharacterEvent(CreatedCharacterEventDTO characterToCreate) {
        try {
            if (existsByMessageId(characterToCreate)) {
                log.info("[CharacterServiceImpl::saveCharacterCreatedEvent] message with id {} and character name {} was already processed OK",
                        characterToCreate.getMessageId(), characterToCreate.getName());
                return;
            }

            callRemoteService();

            saveCreatedEvent(characterToCreate);
            log.info("[CharacterServiceImpl::saveCharacterCreatedEvent] saved character created event with message id {}, character id {} and character name {}",
                    characterToCreate.getMessageId(), characterToCreate.getCharacterId(), characterToCreate.getName());
        } catch (ResourceAccessException exception) {
            log.error("[CharacterServiceImpl::saveCharacterCreatedEvent] error accessing url {}, error {}", ENDPOINT_URL, exception.getMessage());
            throw new RetryableException(exception.getMessage());
        } catch (DataIntegrityViolationException exception) {
            log.error("[CharacterServiceImpl::saveCharacterCreatedEvent] error accessing DB, error message {}", exception.getMessage(), exception);
            throw new NotRetryableException(exception.getMessage());
        }
    }

    private boolean existsByMessageId(CreatedCharacterEventDTO characterToCreate) {
        return createdCharacterEventRepository.existsByMessageId(characterToCreate.getMessageId());
    }

    private void saveCreatedEvent(CreatedCharacterEventDTO characterToCreate) {
        createdCharacterEventRepository.save(buildCreatedCharacterEventEntity(characterToCreate));
    }

    private CreatedCharacterEventEntity buildCreatedCharacterEventEntity(CreatedCharacterEventDTO characterToCreate) {
        return new CreatedCharacterEventEntity
                (characterToCreate.getMessageId(),
                        characterToCreate.getCharacterId()
                        , characterToCreate.getName(),
                        characterToCreate.getCharacterType());
    }

    private void callRemoteService() {
        ResponseEntity<String> response =
                restTemplate.exchange(ENDPOINT_URL, HttpMethod.GET, null, String.class);
        log.info("[CharacterServiceImpl::callRemoteService] response status {}", response.getStatusCode().value());
    }

}
