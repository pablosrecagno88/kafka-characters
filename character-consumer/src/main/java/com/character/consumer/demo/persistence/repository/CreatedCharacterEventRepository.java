package com.character.consumer.demo.persistence.repository;

import com.character.consumer.demo.persistence.entity.CreatedCharacterEventEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CreatedCharacterEventRepository extends JpaRepository<CreatedCharacterEventEntity,Long> {
    boolean existsByMessageId(String messageId);
}
