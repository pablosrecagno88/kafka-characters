package com.core.dev.error;

import java.util.Date;

public class ErrorMessage {
    private String message;
    private Date timestamp;
    private String url;

    public ErrorMessage(){
    }
    public ErrorMessage(String message, Date timestamp, String url) {
        this.message = message;
        this.timestamp = timestamp;
        this.url = url;
    }

    public String getMessage() {
        return message;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public String getUrl() {
        return url;
    }
}
