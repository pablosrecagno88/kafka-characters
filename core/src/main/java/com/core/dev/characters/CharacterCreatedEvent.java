package com.core.dev.characters;

public class CharacterCreatedEvent {
    private String characterId;
    private String name;

    private CharacterType characterType;

    public CharacterCreatedEvent(){
    }

    public CharacterCreatedEvent(String characterId, String name, CharacterType characterType) {
        this.characterId = characterId;
        this.name = name;
        this.characterType = characterType;
    }

    public String getCharacterId() {
        return characterId;
    }

    public void setCharacterId(String characterId) {
        this.characterId = characterId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CharacterType getCharacterType() {
        return characterType;
    }

    public void setCharacterType(CharacterType characterType) {
        this.characterType = characterType;
    }

    @Override
    public String toString() {
        return "CharacterCreatedEvent{" +
                "characterId='" + characterId + '\'' +
                ", name='" + name + '\'' +
                ", characterType=" + characterType +
                '}';
    }
}
