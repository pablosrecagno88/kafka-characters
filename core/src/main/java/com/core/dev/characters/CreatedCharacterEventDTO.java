package com.core.dev.characters;

public class CreatedCharacterEventDTO {

    private String messageId;
    private String characterId;
    private String name;
    private CharacterType characterType;

    public CreatedCharacterEventDTO() {
    }

    public CreatedCharacterEventDTO(String messageId, String characterId, String name, CharacterType characterType) {
        this.messageId = messageId;
        this.characterId = characterId;
        this.name = name;
        this.characterType = characterType;
    }

    public String getMessageId() {
        return messageId;
    }

    public String getCharacterId() {
        return characterId;
    }

    public String getName() {
        return name;
    }

    public CharacterType getCharacterType() {
        return characterType;
    }
}
