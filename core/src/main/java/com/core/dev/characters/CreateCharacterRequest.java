package com.core.dev.characters;

public class CreateCharacterRequest {
    private String name;

    private CharacterType characterType;

    public CreateCharacterRequest() {
    }

    public CreateCharacterRequest(String name, CharacterType characterType) {
        this.name = name;
        this.characterType = characterType;
    }

    public String getName() {
        return name;
    }

    public CharacterType getCharacterType() {
        return characterType;
    }
}
