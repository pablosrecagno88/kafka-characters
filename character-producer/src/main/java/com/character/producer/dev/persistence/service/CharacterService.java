package com.character.producer.dev.persistence.service;


import com.core.dev.characters.CreateCharacterRequest;

public interface CharacterService {
    String createCharacter(CreateCharacterRequest characterData) throws Exception;
}
