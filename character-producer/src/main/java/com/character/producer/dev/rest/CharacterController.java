package com.character.producer.dev.rest;

import com.character.producer.dev.error.CharacterAlreadyExistsException;
import com.character.producer.dev.persistence.service.CharacterService;
import com.core.dev.characters.CreateCharacterRequest;
import com.core.dev.error.ErrorMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/character")
@Slf4j
public class CharacterController {

    private final CharacterService characterService;

    public CharacterController(CharacterService characterService) {
        this.characterService = characterService;
    }

    @PostMapping
    public ResponseEntity<Object> createCharacter(@RequestBody CreateCharacterRequest createCharacterRequest) {
        log.info("[CharacterController::createCharacter] character to create {}", createCharacterRequest.getName());
        String characterId;
        try {
            characterId = characterService.createCharacter(createCharacterRequest);
        }
        catch (CharacterAlreadyExistsException exception){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(buildErrorMessage(exception.getMessage()));
        }
        catch (Exception exception) {
            log.error("[CharacterController::createCharacter] there was an error trying to create character {}, error message {}"
                    , createCharacterRequest.getName(), exception.getMessage(), exception);

            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE.value()).body(buildErrorMessage(exception.getMessage()));
        }

        return ResponseEntity.status(HttpStatus.CREATED.value()).body(characterId);
    }

    private ErrorMessage buildErrorMessage(String errorMessage) {
        return new ErrorMessage(errorMessage, new Date(), "/character");
    }
}
