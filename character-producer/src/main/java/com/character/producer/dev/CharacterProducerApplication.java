package com.character.producer.dev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CharacterProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CharacterProducerApplication.class, args);
	}

}
