package com.character.producer.dev.error;

public class CharacterAlreadyExistsException extends RuntimeException{
    public CharacterAlreadyExistsException(String message) {
        super(message);
    }
}
