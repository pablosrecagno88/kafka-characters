package com.character.producer.dev.persistence.entity;

import com.core.dev.characters.CharacterType;
import jakarta.persistence.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@Entity
@Table(name = "characters")
public class CharacterEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    @Column(nullable = false, unique = true)
    private String name;
    @Enumerated(EnumType.STRING)
    private CharacterType characterType;

    public CharacterEntity(){
    }

    public CharacterEntity(String name, CharacterType characterType) {
        this.name = name;
        this.characterType = characterType;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public CharacterType getCharacterType() {
        return characterType;
    }
}
