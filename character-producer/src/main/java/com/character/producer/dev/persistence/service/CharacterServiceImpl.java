package com.character.producer.dev.persistence.service;


import com.character.producer.dev.error.CharacterAlreadyExistsException;
import com.character.producer.dev.persistence.entity.CharacterEntity;
import com.character.producer.dev.persistence.repository.CharacterRepository;
import com.core.dev.characters.CharacterCreatedEvent;
import com.core.dev.characters.CharacterType;
import com.core.dev.characters.CreateCharacterRequest;
import com.core.dev.common.Topics;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@Slf4j
public class CharacterServiceImpl implements CharacterService {

    private final KafkaTemplate<String, CharacterCreatedEvent> kafkaTemplate;
    private final CharacterRepository characterRepository;

    public CharacterServiceImpl(KafkaTemplate<String, CharacterCreatedEvent> kafkaTemplate, CharacterRepository characterRepository) {
        this.kafkaTemplate = kafkaTemplate;
        this.characterRepository = characterRepository;
    }

    @Override
    public String createCharacter(CreateCharacterRequest characterData) throws Exception {

        String characterName = characterData.getName().toLowerCase();
        CharacterType characterType = characterData.getCharacterType();

        checkCharacterAlreadyExists(characterName);
        String savedCharacterId = characterRepository.save(buildCharacterEntity(characterName, characterType)).getId().toString();
        SendResult<String, CharacterCreatedEvent> sendResult = kafkaTemplate.send(buildProducerRecord(savedCharacterId, characterData)).get();

        log.info("[CharacterServiceImpl::createCharacter] topic {}", sendResult.getRecordMetadata().topic());
        log.info("[CharacterServiceImpl::createCharacter] partition {}", sendResult.getRecordMetadata().partition());
        log.info("[CharacterServiceImpl::createCharacter] partition offset {}", sendResult.getRecordMetadata().offset());

        return savedCharacterId;
    }

    private void checkCharacterAlreadyExists(String characterName) {
        if (wasCharacterCreated(characterName)) {
            String errorMessage = String.format("Character with name %s was already created", characterName);
            log.error(errorMessage);
            throw new CharacterAlreadyExistsException(errorMessage);
        }
    }

    private ProducerRecord<String, CharacterCreatedEvent> buildProducerRecord(String characterId, CreateCharacterRequest characterData) {

        String messageKey = buildUUID();
        String messageId = buildUUID();
        CharacterCreatedEvent event = new CharacterCreatedEvent(characterId, characterData.getName(), characterData.getCharacterType());
        ProducerRecord<String, CharacterCreatedEvent> producerRecord = new ProducerRecord<>(Topics.CHARACTER_CREATED_EVENT_TOPIC, messageKey,event);

        producerRecord.headers().add("messageId", messageId.getBytes());

        return producerRecord;
    }

    private CharacterEntity buildCharacterEntity(String characterName, CharacterType characterType) {
        return new CharacterEntity(characterName, characterType);
    }

    private boolean wasCharacterCreated(String characterName) {
        return characterRepository.existsByName(characterName);
    }

    private String buildUUID(){
        return UUID.randomUUID().toString();
    }
}
