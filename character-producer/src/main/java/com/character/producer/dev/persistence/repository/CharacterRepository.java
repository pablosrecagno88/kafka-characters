package com.character.producer.dev.persistence.repository;

import com.character.producer.dev.persistence.entity.CharacterEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface CharacterRepository extends JpaRepository<CharacterEntity, UUID> {
    boolean existsByName(String name);
}
